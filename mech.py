import mechanize

mech=mechanize.Browser()
mech.set_handle_robots(False)
mech.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    

flag=True
tries=10
skip_fail=True
while flag and tries:
    try:
        mech.open('https://ssindeks.kompas.com/terpopuler/2019-10-01')
        flag=False
    except:
        tries-=1
        if(tries==0):
            skip_fail=False
        print("time out")

if(skip_fail):
    for link in mech.links(url_regex='read'):
        print(link.url)