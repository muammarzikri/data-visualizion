# crawl with   

import mechanize
import sys
import re
import time
import os
import json
from bs4 import BeautifulSoup
from datetime import datetime,timedelta

mech=mechanize.Browser()
mech.set_handle_robots(False)
mech.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

URL={}
COMPLETE_URL={}
program={}

# make date with counter 
def makeDate(formatDate,days):
    now = datetime.now()
    return (now.today()-timedelta(days)).strftime(formatDate[0])

# prosedure untuk normalize format dan generate url
# get date{+:%m-%d-%Y:+} from url and replace
# ex : berita/date{+:%m-%d-%Y:+} menjadi berita/04-12-2019 sesuai counter 
def makeUrl(urlSeed,counter,regex):
    global URL
    print("makeUrl",urlSeed,counter,regex)

    formatUrl=re.findall('%(.*)%',urlSeed)
    for pattern in formatUrl:
        if(re.match("date*",pattern)):
            pattern=makeDate(re.findall('{\+:(.*):\+}',pattern),counter)
            formatUrl=re.sub("\%date{\+:(.*):\+}%",pattern,urlSeed)


    print("formatUrl",formatUrl)
    time_publish=pattern
    flag=True
    tries=10 #try for x times
    skip_fail=True
    # try for time out 
    while flag and tries:
        try:
            try:
                mech.open(formatUrl)
                flag=False
            except IOError as e:
                print ("Network Error ... sleep for 3s")
                time.sleep(3)
            except:
                tries-=1
                if isinstance(mechanize.HTTPError):
                    print (mechanize.HTTPError.code)
                else:
                    print (mechanize.HTTPError.reason.args)
                if(tries==0):
                    skip_fail=False
                print("time out")
        except IOError as e:
            print ("Network Error ... sleep for 3s")
            time.sleep(3)

    
    if(skip_fail):
        for link in mech.links(url_regex=regex):
            if(link.url not in URL):
                # ganti nilai satu dengan waktu * timestemp
                print(len(URL),"get",counter,link.url)
                URL[link.url]={
                    "counter":1,
                    "time-publish":time_publish
                }
                if(len(URL)%100==0):
                    print("100 link get writeUrl ")
                    writeUrl(URL,program['-save'])
            else:
                print("old",counter,link.url)
                URL[link.url]["counter"]+=1
                URL[link.url]["time_get_link_last"]=datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            mech.follow_link()  # takes EITHER Link instance OR keyword args
            mech.back()

def writeUrl(url,dst):
    file=open(dst,"w+")
    file.write(json.dumps(url))
    file.close()
    print("writeUrl",url,dst)

def writePage(html,dst):
    file=open(dst,"w+")
    file.write(html)
    file.close()

def loadURLs(pathUrl):
    if(os.path.isfile(pathUrl)):
        file=open(pathUrl,"r")
        url=json.loads(file.read())
        file.close()
    else:
        url={}
    return url

def getTitleFromHTML(html):
    soup = BeautifulSoup(html)
    return soup.title.string 

def uniqTime():
    return datetime.now().strftime('%Y:%m:%d:%H:%M:%S')

# Python code to merge dict using a single  
# expression
def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def crawl(url,name,time_publish):
    global COMPLETE_URL
    output = os.popen('wget -qO- '+str(url)).read()
    html_url="<!-- url='"+url+"' -->"
    time="<!-- time-crawl='"+datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"' -->"
    time="<!-- time-publish='"+time_publish+"' -->"
    COMPLETE_URL[url]={
        'file':name,
        'time':datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    }
    return html_url+time+output

def getSeed(seedFile):
    seed={}
    f = open(seedFile, "r")

    for x in f:
        line=x.split(' ')
        seed[line[0].strip()]={
            "counter":line[1].strip(),
            "regex":line[2].strip()
        }

    return seed

def crawl_link(cmd):
    message="please set value for "
    flag=1

    if('-seed' not in cmd):
        message+="-seed seed-file "
        flag=0

    if('-url' not in cmd):
        message+="-url exist-url-file "
        flag=0

    if('-save' not in cmd):
        message+="-save save-url-file "
        flag=0

    if(flag==0):
        print(message)    
        return

    seed=getSeed(cmd['-seed'])
    global URL
    URL=loadURLs(cmd['-url'])
    for key in seed.keys():
        print("seed key",key)

        for counter in range(0,int(seed[key]['counter'])):
            makeUrl(key,counter,seed[key]['regex'])
            # html=crawl(url)
            # print(html)
            # write(html,"dst")
 
    writeUrl(URL,cmd['-save'])

def crawl_page(cmd):
    message="please set value for "
    flag=1

    if('-target' not in cmd):
        message+="-target target-url-file "
        flag=0

    if('-dst' not in cmd):
        message+="-dst dst-path "
        flag=0

    if('-complete' not in cmd):
        message+="-complete complete-url-file "
        flag=0

    if(flag==0):
        print(message)    
        return

    global URL
    global COMPLETE_URL
    
    URL=loadURLs(cmd['-target'])
    COMPLETE_URL=loadURLs(cmd['-complete'])
    counter=1
    for key in URL.keys():
        print(counter,len(URL),"crawl ",key)
        html=crawl(key,str(counter)+".html",URL[key]['time-publish'])
        writePage(html,cmd['-dst']+str(counter)+"-"+uniqTime()+".html")
        writeUrl(COMPLETE_URL,cmd['-complete'])
        counter+=1


def main():
    global program
    counter=1
    # for get cmd with -flag
    for cmd in sys.argv[1:]:
        if(cmd[:1]=='-' and counter+1<len(sys.argv)):
            program[cmd]=sys.argv[counter+1]    
        counter+=1

    if('-mode' not in program):
        print("please set value for run mode: ... -mode crawl-link|crawl-page")
        return

    if(program['-mode']=='crawl-page'):
        print('crawl-page')    
        crawl_page(program)
    elif(program['-mode']=='crawl-link'):
        print('crawl-link')
        crawl_link(program)

main()
# file.py seed url_file_load url_file_save
