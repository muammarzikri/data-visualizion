import json
import os
import sys
import re

data={}
program={}
stopwords={}
counter_word=1

def urlify(s):
    # Remove all non-word characters (everything except numbers and letters)
    s = re.sub(r"[^\w\s\.\-\/]+", ' ', s)
    s = re.sub(r'([0-9])[\.]+([a-zA-Z])', r'\1 \2', s)
    s = re.sub(r'([a-z])[\.]+([a-zA-Z])', r'\1 \2', s)
    s = re.sub(r"([a-z0-9])\.[\s]", r'\1 ', s)
    s = re.sub(r"[\s]+\.", ' ', s)
    s = re.sub(r"\.$", ' ', s)
    s = re.sub(r"\s\-\s", ' ', s)

    # Replace all runs of whitespace with a single dash
    s = re.sub(r"\s+", ' ', s)
    s = re.sub(r"\s$", '', s)
    return s

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def constructDictFromJson(jsonFile):
    return json.loads(readFile(jsonFile))

def readFile(pathFile):
    return os.popen('cat '+str(pathFile)).read()

def getAllFilesInPath(pathFolder):
    return os.popen('ls '+str(pathFolder)).read().split('\n')

def loadStropWords(fileStopWords):
    global stopwords
    stopwords_data=readFile(fileStopWords).split('\n')
    for stopword in stopwords_data :
        stopwords[stopword]=1

def isset(variable):
	return variable in locals() or variable in globals()

def saveFile(dst,text):
    file=open(dst,"w+")
    file.write(json.dumps(text))
    file.close()

def splitWord(texts):
    words=[]
    multi_text=['PT.']
    multi_text=dict.fromkeys(multi_text) 
    flag=False    
    tmp_key=""
    texts=urlify(texts)
    for word in texts.split(" "):
        if(flag):
            if(word[0].islower):
                flag=False
            else:
                word[tmp_key]+=" "+word
                continue

        if(word in multi_text):
            flag=True
            tmp_key=word
        words.append(word)
    return words          

def countTerm(dataClean):
    global data
    global stopwords
    global counter_word
    countWord=1
    for word in splitWord(dataClean['title']+" "+dataClean['text']):
        if not hasNumbers(word):
            
            word=word.lower()
            if(len(word)==0):
                continue
            if '-stopwords' in program:
                if word in stopwords :
                    stopwords[word]+=1
                    continue
            countWord+=1
            if word not in data:
                counter_word+=1
                data[word]={
                    'count':1,
                    'list':{
                        dataClean['time'].split(" ")[0]:1
                    }
                }
            else:
                data[word]['count']+=1
                time=dataClean['time'].split(" ")[0]
                if time in data[word]['list']:
                    data[word]['list'][time]+=1
                else:
                    data[word]['list'][time]=1

    print("count word",countWord,end=' ')

# data file-hasil
def main():
    global data
    global program
 
    counter=1
    # for get cmd with -flag
    for cmd in sys.argv[1:]:
        if(cmd[:1]=='-' and counter+1<len(sys.argv)):
            program[cmd]=sys.argv[counter+1]
        counter+=1

    for i in ['-src','-save']:
        if(i not in program):
            print("please set value for run mode: ... -src pathFolder -save saveData.json [-stopwords stopwords-file]")
            return

    if '-stopwords' in program:
        loadStropWords(program['-stopwords'])

    listFileClean=getAllFilesInPath(program['-src'])
    counterFile=1
    for cleanHtmlFile in listFileClean:
        if(cleanHtmlFile==""):
            continue
        cleanData=constructDictFromJson(program['-src']+'/'+cleanHtmlFile)
        if(cleanData['time']!=""):
            print("Open file",counterFile,len(listFileClean),end='\t')
            countTerm(cleanData)
            print(counter_word)
            counterFile+=1

    saveFile(program['-save'],data)
    if '-stopwords' in program:
        saveFile(program['-save']+".stopwords",stopwords)

# print(url)
main()