# cleaning
# by muammar zikri aksana 
import os
from bs4 import BeautifulSoup
from datetime import datetime,timedelta
import sys
import re
import json

program={}
tagsFilters=[]
textFilters=[]

def readFile(pathFile):
    return os.popen('cat '+str(pathFile)).read()

def urlify(s):
    # Remove all non-word characters (everything except numbers and letters)
    s = re.sub(r"[^\w\s\.\-\/]+", ' ', s)
    s = re.sub(r'([0-9])[\.]+([a-zA-Z])', r'\1 \2', s)
    s = re.sub(r'([a-z])[\.]+([a-zA-Z])', r'\1 \2', s)
    s = re.sub(r"([a-z0-9])\.[\s]", r'\1 ', s)
    s = re.sub(r"[\s]+\.", ' ', s)
    s = re.sub(r"\.$", ' ', s)
    s = re.sub(r"\s\-\s", ' ', s)

    # Replace all runs of whitespace with a single dash
    s = re.sub(r"\s+", ' ', s)
    s = re.sub(r"\s$", '', s)
    return s

def readFilter(pathFileTags,pathFileText):
    global tagsFilters
    global textFilters

    fileFilter=os.popen('cat '+str(pathFileTags)).read()
    for line in fileFilter.split("\n"):
        tagsFilters.append(line)

    fileFilter=os.popen('cat '+str(pathFileText)).read()
    for line in fileFilter.split("\n"):
        textFilters.append(line)


def saveFile(text,dst):
    file=open(dst,"w+")
    file.write(json.dumps(text))
    file.close()
    print("saveFile",text['url'])

# file format 
#   url html
#   time crawl
#   time cleaning
#   title
#   tags
#   text

def createFileFormat(urlV,timeV,titleV,tagsV,textV):
    html={
        'url':urlV,
        'time':timeV,
        'title':titleV,
        'tags':tagsV,
        'text':urlify(textV)
    }
    return html

def getAllFilesInPath(pathFolder):
    return os.popen('cat '+str(pathFolder)).read()

def getTags(soup):
    tags={}
    counter=0
    print("\thref",end='')
    for filterHtml in tagsFilters:
        filterHtmlArr=filterHtml.split('\ ')
        if(len(filterHtmlArr)>=2):
            for a in soup.findAll(filterHtmlArr[0], json.loads(filterHtmlArr[1]),href=True):
                href=a['href']
                if(re.match(filterHtmlArr[2],href)):
                    print("",counter, end = '')
                    tags[href]=1
                    counter+=1

    return tags

def getText(soup):
    text=[]
    counter=0
    print("\thref",end='')
    for filterHtml in textFilters:
        filterHtmlArr=filterHtml.split('\ ')
        for textInner in soup.findAll(filterHtmlArr[0], json.loads(filterHtmlArr[1])):
            for textp in textInner.findAll('p', recursive=False):
                text.append(textp.text)

    return ''.join(text)

def getTime(html):
    if(re.search("<meta property=\"article:published_time\" content=\"",html)):
        time=re.findall("<meta property=\"article:published_time\" content=\"(.*?)\"",html)[0]
        return time
    elif(re.search("<meta name=\"content_PublishedDate\" content=\"",html)):
        time=re.findall("<meta name=\"content_PublishedDate\" content=\"(.*?)\"",html)[0]
        return time

def cleanHTML(html):
    soup = BeautifulSoup(html,features="lxml")
    tags = getTags(soup)
    text = getText(soup)
    time = getTime(html)
    try:
        title=re.findall('\<title\>(.*)\<\/title\>',html)[0]
    except :
        return False
    return createFileFormat(
        re.findall('\<\!\-\-\ url\=\'([^\>]*)\'\ \-\-\>',html)[0],
        time,
        title,
        tags,
        text
    )

def isset(variable):
	return variable in locals() or variable in globals()
# print("print",readFile('/home/mza/Documents/pro/bigdata/tugas/data/html/9.html'))

def getName(fullPathFile):
    path=fullPathFile.split("/")
    return path[len(path)-1]


def main():
    global program
    counter=1
    # for get cmd with -flag
    for cmd in sys.argv[1:]:
        if(cmd[:1]=='-' and counter+1<len(sys.argv)):
            program[cmd]=sys.argv[counter+1]
        counter+=1

    print(program)
    for i in ['-src','-dst','-filter-tags','-filter-text']:
        if(i not in program):
            print("please set value for run mode: ... -src pathFolder -dst pathFolder -filter-tags file-filter -filter-text file-text-filter")
            return

    listOfFiles=getAllFilesInPath(program['-src'])

    readFilter(program['-filter-tags'],program['-filter-text'])

    counter=0
    hmtlFiles=listOfFiles.split('\n')
    print(hmtlFiles)
    for hmtlFile in hmtlFiles:
        if(len(hmtlFile)<1):
            continue
        print(counter,len(hmtlFiles))
        html=readFile(hmtlFile)
        html=cleanHTML(html)
        if(html==False):
            continue
        
        saveFile(html,program['-dst']+"/"+getName(hmtlFile))
        counter+=1
        # print(html)

main()