# Tugas2

> Format penulisan link pada seed file
> terdiri dari 3 segment : format,counter,regex
> setiap link dipisah dengan newline
link : counter , link ditulis dengan format ex : berita/12-21-2019 menjadi : berita/%date{+:%m-%d-%Y:+}%/

1. Untuk melakukan crawling dibutuhkan 2 file, seed file sebagai target dan url file sebagai list dari url yang telah didownload file boleh berisi {}

2. Untuk melakukan cleaning src data bukanlah folder tapi file yang berisikan list dari path file yang akan diclean , ini dimaksudkan agar program cleaning dapat dijalan kan serentak untuk target file berbeda dalam direktori yang sama.

3. Untuk menggenerate data dibutuhkan file cleaning dalam format json, yang terdiri dari minum key, : title, time, url, tags, text. Hasil dari generate data adalah file json dengan format hasil
    data.json

    ```json

        {
            word1:{
                ‘count’ : jumlah-kata-yang-ditemukan-disemua-file-clean,
                ‘list’:{
                    ‘2019-01-29’ : jumlah-kata-yang-ditemukan-di-tgl-ini,
                    ‘2019-01-28’ : 13
                }

            },
            word2:{
                ‘count’ : 404834,
                ‘list’:{
                    ‘2019-01-29’ : 3,
                    ‘2019-01-28’ : 13,
                    '…':…,
                    ‘2001-01-28’ : 9
                    }

            },
            '…':…,
                word-n:{
                    ‘count’ : n,
                    ‘list’:{
                        ‘2019-01-29’ : 3,
                        ‘2019-01-28’ : 13,
                        '…':…,
                        ‘yyyy-mm-dd’ : 9
                }

            }
        }
    ```
