import json
import os
import sys
from collections import OrderedDict
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
from datetime import datetime,timedelta


data={}
program={}

def makeDate(formatDate,date,dec):
    now = datetime.strptime(date,formatDate)
    return (now-timedelta(dec)).strftime(formatDate)

def orderDesc(data):
   return OrderedDict(sorted(data.items(), key=lambda kv: kv[1]['count'], reverse=True))

def constructDictFromJson(jsonFile):
    return json.loads(readFile(jsonFile))

def readFile(pathFile):
    return os.popen('cat '+str(pathFile)).read()

def grafik1(size):
    global data
    labels = []
    data_grafik=[]
    counter=1
    for key in data:
        if(counter>size):
            break
        labels.append(key)
        data_grafik.append(data[key]['count'])
        counter+=1
   
    # bars are by default width 0.8, so we'll add 0.1 to the left coordinates
    # so that each bar is centered
    xs = [i + 0.5 for i, _ in enumerate(labels)]
    # plot bars with left x-coordinates [xs], heights [num_oscars]
    r=0.2
    b=0.4
    g=0.6
    plt.rcParams["figure.figsize"] = (9, 5)
    plt.xticks(rotation=35)
    plt.bar(xs, data_grafik,color=(r,g,b))
    plt.ylabel("# Counts")
    plt.xlabel("# Words")
    plt.title("Words Rank")
    # label x-axis with movie names at bar centers

    plt.xticks([i + 0.5 for i, _ in enumerate(labels)], labels)
    # plt.legend(labels)
    plt.show()


def grafik2(top,maxC,startDate):
    global data
    labels = []
    data_grafik={}
    counter_top=1
    flag=True

    top_word=[]
    for key in data:
        if(counter_top>top):
            break
        top_word.append(key)
        for counter in range(maxC,0,-1):
            date=makeDate("%Y-%m-%d",startDate,counter)
            if date in data[key]['list']:
                if date not in data_grafik:
                    data_grafik[date]={}
                data_grafik[date][key]=data[key]['list'][date]
            else:
                if date not in data_grafik:
                    data_grafik[date]={}
                data_grafik[date][key]=0
            if(flag):
                labels.append(date)
            # data_grafik[date]=data_grafik_build
        flag=False
        counter_top+=1


    for word in top_word:
        data_plt=[]

        for key in data_grafik:
            data_plt.append(data_grafik[key][word])

        plt.plot(labels, data_plt, label = word)

    plt.xlabel('Date')
    plt.rcParams["figure.figsize"] = (13, 6)
    # Set the y axis label of the current axis.
    plt.ylabel('Count')
    # Set a title of the current axes.
    plt.title('Words Rank')
    plt.xticks(rotation=35)

    # show a legend on the plot
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    # Display a figure.
    # plt.savefig('im/grafik2.png')
    plt.show()
    # plt.gca().clear()

def func(pct, allvals):
    absolute = int(pct/100.*np.sum(allvals))
    return "{:.1f}%\n({:d})".format(pct, absolute)

def grafik3(size):
    global data
    labels = []
    data_grafik=[]
    counter=1

    for key in data:
        if(counter>size):
            break
        labels.append(key)
        data_grafik.append(data[key]['count'])
        counter+=1

    ingredients = [x.split()[-1] for x in labels]
    fig, ax = plt.subplots(figsize=(13, 5), subplot_kw=dict(aspect="equal"))
    wedges, texts, autotexts = ax.pie(data_grafik, autopct=lambda pct: func(pct, data_grafik),
                                  textprops=dict(color="w"),pctdistance=0.75)
    ax.legend(wedges, ingredients,
          title="Ingredients",
          loc="center left",
          bbox_to_anchor=(1, 0, 0.5, 1))
    plt.setp(autotexts, size=8,weight="bold")
    ax.set_title("Words Counts")
    plt.show()


def main():
    global data
    global program
 
    counter=1
    # for get cmd with -flag
    for cmd in sys.argv[1:]:
        if(cmd[:1]=='-' and counter+1<len(sys.argv)):
            program[cmd]=sys.argv[counter+1]
        counter+=1

    for i in ['-data']:
        if(i not in program):
            print("please set value for run mode: ... -data data-file.json [-top toprank] [-start date] [-dur duration]")
            return

    data=constructDictFromJson(program['-data'])
    data=orderDesc(data)
    counter=1
    maxCount=10
    duration=14
    date=datetime.now().strftime("%Y-%m-%d")

    if '-top' in program:
        maxCount=int(program['-top'])
    if '-dur' in program:
        duration=int(program['-dur'])
    if '-start' in program:
        date=program['-start']
    
    grafik1(maxCount)
    grafik2(maxCount,duration,date)
    grafik3(maxCount)

    
main()